module.exports = `
Usage:
 # if you want change version 
    ver  up|down  major|minor|patch

 # if you want to know version of your app package
    ver  current
    
 # if you want to know version of this package
    ver  --version
   
You can use "ver" command or "version-changer" - is similar commands. 
   
Options:
	up      - increment version up
	down    - increment version down
	major   - first literal  (X.*.*)
	minor   - second literal (*.X.*)
	patch   - third literal  (*.*.X)
	current - for your current package version display
	
Arguments:	
  --file         - specify another path to package.json file
  --version (-v) - show version of package (WILL ABORT SCRIPT AFTER CALL!)
\n`;
