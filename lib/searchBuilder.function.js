const searchInArray = require("./searchInArray.function");

/**
 * Builder function which return function for searching which returns boolean result of searching values in array
 *
 * @param {Array<string>} sourceArray - array for searching value in
 *
 * @returns {function}
 */
module.exports = (sourceArray) => {
  return (...searchedValues) => searchInArray(sourceArray, ...searchedValues);
};
