/**
 * version string change function
 *
 * @param version {string} - version of package for change
 * @param directives {Array<string>} - array with directives which can contain increment type "patch"|"minor"|"major" and direction: "up"|"down"
 *
 * @returns {Promise<string>} with changed version
 */
module.exports = async (version, directives = ["up", "patch"]) => {

  const versionNames = ["major", "minor", "patch"];

  version = (/^(\d+)\.(\d+)\.(\d+)(.*)/gi).exec(version).splice(1,4);

  for(let i = 0; i<3; i++) {
    if(version[i]) {
      version[i] = parseInt(version[i]);
    } else {
      version[i] = 0;
    }
  }

  for(let key = 0; key < versionNames.length; key++) {
    if(directives.indexOf(versionNames[key]) >= 0) {
      if (directives.indexOf("down") >= 0) {
        version[key] = (version[key] - 1) >= 0 ? version[key]-1 : 0;
      } else {
        version[key]++;
      }

      for(key++; key < versionNames.length; key++) {
        version[key] = 0;
      }
    }
  }

  for(let key = 1; key < versionNames.length; key++) {
    version[key] = "." + version[key];
  }

  // process.stdout.write(`Changing version ${pkg.version} ==> ${version.join("")} `);

  return version.join("");
};
