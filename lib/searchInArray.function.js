/**
 * Function which return boolean result of searching values in array
 *
 * @param {Array<string>} sourceArray - array for searching value in
 * @param {Array<string>} searchedValues - array with values for search
 *
 * @returns {boolean}
 */
module.exports = (sourceArray, ...searchedValues) => {
  for(let i =0; i < searchedValues.length; i++) {
    if(sourceArray.filter( (v) => (v === searchedValues[i]) ).length > 0) {
      return true;
    }
  }

  return false;
};
