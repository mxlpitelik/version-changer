#!/usr/bin/env node

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });

const mri = require("mri");
const fs = require("fs");
const changeVersion = require("../lib/changeVersion.function");
const help = require("../lib/help");
let fileName = "package.json";
let pkg;

const argv = mri(process.argv.slice(2), {
  boolean: [
    "help", "h",
    "version", "v",
    "current", "cur"
  ],
  string: [
    "file"
  ]
});

if (argv.file) {
  fileName = argv.file;
  process.stdout.write(`\n\nChanged path to package.json file to [ ${fileName} ]\n\n`);
}

if(fs.existsSync(fileName)) {
  pkg = fs.readFileSync(fileName);
} else {
  process.stdout.write("\n\nERROR!\nFile is does not exists!\n\n");
  process.exit(0);
}

try {
  pkg = JSON.parse(pkg);
} catch (e) {
  process.stdout.write("\n\nERROR!\nJSON format is wrong!\n\n");
  process.exit(0);
}

const S = require("../lib/searchBuilder.function")(argv._);

if (argv.help || argv.h) {
  process.stdout.write(help);
  process.exit(0);
}

if (argv.version || argv.v) {
  const vc = require("../package.json");
  process.stdout.write(`${vc.name} v${vc.version}\n`);
  process.exit(0);
}

if (S("current", "cur")) {
  process.stdout.write(`${pkg.name} v${pkg.version}\n`);
  process.exit(0);
}

if (!S("up", "down")) {
  process.stdout.write(help);

  process.stdout.write("\n\nERROR:\n  MISSED PARAMETER! 'up' or 'down' must be specified!\n\n");

  process.exit(0);
}

if (!S("major", "minor", "patch")) {
  process.stdout.write(help);

  process.stdout.write("\n\nERROR:\n  MISSED PARAMETER! 'major', 'minor' or 'patch' must be specified!\n\n");

  process.exit(0);
}


/**
 * Bootstrap function for operations start
 *
 * @returns {Promise<void>}
 */
const bootstrap = async () => {
  const newVersion = await changeVersion(pkg.version, argv._);
  process.stdout.write(`\n\nChanging version ${pkg.version} ==> ${newVersion}\n`);

  pkg.version = newVersion;

  fs.writeFileSync(fileName, JSON.stringify(pkg, null, 2));

  process.stdout.write(`\nSUCCESS!\n${fileName} successfully rewritten! \n\n`);
};

bootstrap();
