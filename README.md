# version-change

package.json version changer (because npm version is didn't works well).

## How to Up version before each "git push"

Better decision is use `npx` for this, but you also can install it like dev dependency `npm i -D version-changer`

- Add npm scripts block into `package.json`
```
    ...
    "scripts": {
      "ver:up": "npx version-changer@latest up patch && git add package.json && git commit -m \"Version up commit\""
      },
    ...
```
- Install like dev dependency `npm i -D husky`, if not installed yet
- Add husky hook to your `package.json` (you can add this command without npm scripts block, but windows will dont work)
```
    ...
    "husky": {
      "hooks": {
          "pre-push": "npm run ver:up"
        }
      },
    ...
```

##Usage:

#### change version 
    #: ver up|down major|minor|patch

#### Display current version of your package.json
    #: ver current
    
#### Version of this package
    #: ver --version
   
You can use "ver" command or "version-changer" - is similar commands. 
   
##### Options:
	up      - increment version up
	down    - increment version down
	major   - first literal  (X.*.*)
	minor   - second literal (*.X.*)
	patch   - third literal  (*.*.X)
	current - for your current package version display
	
##### Optional arguments:	
    --file         - specify another path to package.json file
    
##### Special arguments:	
    --version (-v) - show version of package (WILL ABORT SCRIPT AFTER CALL!)
      
##### Help
    --help    (-h) - show help
